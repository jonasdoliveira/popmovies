package br.com.jonasdoliveira.popmovies;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Jonas on 22/11/2016.
 */
public class MainFragment extends Fragment {

    private static final String MOST_POPULAR = "popular";
    private static final String TOP_RATED = "top_rated";
    public static final String MOVIE_DATA = "movie_data";
    private static final String GRID_ORDER = "order";
    private static final String GRID_POSITION = "grid_position";

    private GridViewAdapter mMoviesAdapter;
    private GridView mMoviesGridView;
    private String mListOrder = MOST_POPULAR;
    private int mSavedPosition = GridView.INVALID_POSITION;

    public MainFragment() {
    }

    private final String LOG_TAG = MainFragment.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(LOG_TAG, "onCreate");
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if (savedInstanceState != null) {
            mListOrder = savedInstanceState.getString(GRID_ORDER);
            mSavedPosition = savedInstanceState.getInt(GRID_POSITION);
        }

        updateMovies(mListOrder);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        mMoviesAdapter = new GridViewAdapter(getActivity(), R.layout.grid_image_item, new ArrayList());

        mMoviesGridView = (GridView) rootView.findViewById(R.id.grid_movies);
        mMoviesGridView.setAdapter(mMoviesAdapter);

        mMoviesGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(getActivity(), DetailActivity.class);
                MovieData movie = (MovieData) mMoviesAdapter.getItem(position);
                i.putExtra(MOVIE_DATA, movie);
                startActivity(i);
            }
        });

        return rootView;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(GRID_ORDER, mListOrder);
        // Save the average position of the poster movie that is being visible on screen at moment.
        mSavedPosition =
                ((mMoviesGridView.getFirstVisiblePosition() +
                        mMoviesGridView.getLastVisiblePosition()) / 2) + 1;
        outState.putInt(GRID_POSITION, mSavedPosition);

        Log.d(LOG_TAG, "onSaveInstanceState - mSavedPosition: " + mSavedPosition
                + ", mListOrder: " + mListOrder);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main_fragment, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.action_most_popular:
                resetPosition();
                updateMovies(MOST_POPULAR);
                return true;

            case R.id.action_top_rated:
                resetPosition();
                updateMovies(TOP_RATED);
                return true;
        }
        return false;
    }

    /**
     * Resets the {@link #mSavedPosition}.
     * Used when the list of movies is changed or updated.
     */
    private void resetPosition() {
        mSavedPosition = GridView.INVALID_POSITION;
    }

    /**
     * Update the list of movies from server by order passed in param
     *
     * @param order The order to show the movies, can be {@link #MOST_POPULAR} or {@link
     *              #TOP_RATED}
     */
    private void updateMovies(String order) {
        Log.d(LOG_TAG, "updateMovies - order: " + order);
        mListOrder = order;
        FetchMoviesTask fetchMoviesTask = new FetchMoviesTask();
        fetchMoviesTask.execute(order);
    }

    private class FetchMoviesTask extends AsyncTask<String, Void, ArrayList<MovieData>> {

        private final String LOG_TAG = FetchMoviesTask.class.getSimpleName();

        @Override
        protected ArrayList<MovieData> doInBackground(String... orderMode) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;
            String moviesJsonStr = null;

            try {
                // https://api.themoviedb.org/3/movie/popular?api_key=ef30ea2d60a866102f0f203315477399&language=pt-br&page=1
                final String API_KEY = "api_key";
                final String LANGUAGE = "language";
                final String ORDER = (orderMode[0].equals(TOP_RATED) ?
                        TOP_RATED + "?" : MOST_POPULAR + "?");
                final String BASE_URL_POPULAR = "https://api.themoviedb.org/3/movie/" + ORDER;

                final Uri builtUri = Uri.parse(BASE_URL_POPULAR).buildUpon()
                        .appendQueryParameter(API_KEY, BuildConfig.OPEN_MOVIEDB_API_KEY)
                        .appendQueryParameter(LANGUAGE, "pt-br")
                        .build();

                final URL url = new URL(builtUri.toString());

                Log.d(LOG_TAG, "Built URI: " + builtUri.toString());

                // Create the request to MovieDB, and open the connection
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                // Read the input stream into a String
                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder buffer = new StringBuilder();
                if (inputStream == null) {
                    // Nothing to do.
                    moviesJsonStr = null;
                    Log.d(LOG_TAG, "Ignoring - inputStream is NULL");
                } else {
                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        buffer.append(line).append("\n");
                    }

                    if (buffer.length() == 0) {
                        // Stream was empty.  No point in parsing.
                        moviesJsonStr = null;
                    }

                    moviesJsonStr = buffer.toString();
                    //Log.d(LOG_TAG, "moviesJsonStr: " + moviesJsonStr);
                }

            } catch (IOException e) {
                Log.e(LOG_TAG, "Error: ", e);
                moviesJsonStr = null;
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            try {
                return getMoviesList(moviesJsonStr);
            } catch (JSONException e) {
                Log.e(LOG_TAG, e.getMessage(), e);
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(ArrayList<MovieData> movies) {
            if (movies != null) {
                mMoviesAdapter.clear();
                mMoviesAdapter.addAll(movies);

                // When view was destroyed and rebuild need to go to the same previous position,
                // for example rotating the device.
                if (mSavedPosition != GridView.INVALID_POSITION) {
                    mMoviesGridView.smoothScrollToPosition(mSavedPosition);
                    resetPosition();
                } else {
                    // after update scroll to top, initial position
                    mMoviesGridView.smoothScrollToPosition(0);
                }
            }
        }

        private ArrayList<MovieData> getMoviesList(String moviesJsonStr)
                throws JSONException {

            ArrayList<MovieData> moviesList = new ArrayList<>();

            // These are the names of the JSON objects that need to be extracted.
            final String POSTER_PATH = "poster_path";
            final String ADULT = "adult";
            final String OVERVIEW = "overview";
            final String RELEASE_DATE = "release_date";
            final String GENRE_IDS = "genre_ids";
            final String ID = "id";
            final String ORIGINAL_TITLE = "original_title";
            final String ORIGINAL_LANGUAGE = "original_language";
            final String TITLE = "title";
            final String BACKDROP_PATH = "backdrop_path";
            final String POPULARITY = "popularity";
            final String VOTE_COUNT = "vote_count";
            final String VIDEO = "video";
            final String VOTE_AVERAGE = "vote_average";

            final JSONObject moviesRoot = new JSONObject(moviesJsonStr);
            final JSONArray results = moviesRoot.getJSONArray("results");

            for (int i = 0; i < results.length(); i++) {
                final MovieData movie = new MovieData();

                // Get the JSON object representing the moview
                JSONObject movieJson = results.getJSONObject(i);
                // Set attributes
                movie.setPosterPath(movieJson.getString(POSTER_PATH));
                movie.setAdult(movieJson.getBoolean(ADULT));
                movie.setOverview(movieJson.getString(OVERVIEW));
                movie.setReleaseDate(movieJson.getString(RELEASE_DATE));
                movie.setGenreIDs(movieJson.getJSONArray(GENRE_IDS));
                movie.setId(movieJson.getInt(ID));
                movie.setOriginalTitle(movieJson.getString(ORIGINAL_TITLE));
                movie.setOriginalLanguage(movieJson.getString(ORIGINAL_LANGUAGE));
                movie.setTitle(movieJson.getString(TITLE));
                movie.setBackdropPath(movieJson.getString(BACKDROP_PATH));
                movie.setPopularity(movieJson.getString(POPULARITY));
                movie.setVoteCount(movieJson.getInt(VOTE_COUNT));
                movie.setHasVideo(movieJson.getBoolean(VIDEO));
                movie.setVoteAverage(movieJson.getDouble(VOTE_AVERAGE));

                moviesList.add(movie);
            }

            return moviesList;
        }
    }
}