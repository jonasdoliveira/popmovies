package br.com.jonasdoliveira.popmovies;

import org.json.JSONArray;
import java.io.Serializable;

/**
 * Created by Jonas on 13/12/2016.
 */

class MovieData implements Serializable {

    private static final long serialVersionUID = 6402501088508081852L;

    private String posterPath;
    private boolean isAdult;
    private String overview;
    private String releaseDate;
    private int[] genreIDs;
    private int id;
    private String originalTitle;
    private String originalLanguage;
    private String title;
    private String backdropPath;
    private String popularity;
    private int voteCount;
    private boolean hasVideo;
    private double voteAverage;

    String getPosterPath() {
        return posterPath;
    }

    void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public boolean isAdult() {
        return isAdult;
    }

    void setAdult(boolean adult) {
        isAdult = adult;
    }

    String getOverview() {
        return overview;
    }

    void setOverview(String overview) {
        this.overview = overview;
    }

    String getReleaseDate() {
        return releaseDate;
    }

    void setReleaseDate(String releaseDate) {
        // date received yyyy-mm-dd, changed to dd/mm/yyyy
        final String year = releaseDate.substring(0,4);
        final String month = releaseDate.substring(5,7);
        final String day = releaseDate.substring(8);

        this.releaseDate = day + "/" + month + "/" + year;
    }

    public int[] getGenreIDs() {
        return genreIDs;
    }

    void setGenreIDs(JSONArray genreJsonArray) {
        int[] genreArray = new int[genreJsonArray.length()];
        for (int i = 0; i < genreJsonArray.length(); i++) {
            genreArray[i] = genreJsonArray.optInt(i);
        }
        this.genreIDs = genreArray;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    String getTitle() {
        return title;
    }

    void setTitle(String title) {
        this.title = title;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getPopularity() {
        return popularity;
    }

    void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public int getVoteCount() {
        return voteCount;
    }

    void setVoteCount(int voteCount) {
        this.voteCount = voteCount;
    }

    public boolean isHasVideo() {
        return hasVideo;
    }

    void setHasVideo(boolean hasVideo) {
        this.hasVideo = hasVideo;
    }

    double getVoteAverage() {
        return voteAverage;
    }

    void setVoteAverage(double voteAverage) {
        this.voteAverage = voteAverage;
    }
}
