package br.com.jonasdoliveira.popmovies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private MovieData mMovieData;

    private ImageView imgMovie;
    private TextView tvMovieName;
    private TextView tvYear;
    private TextView tvRate;
    private TextView tvOverview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mMovieData = (MovieData) getIntent().getSerializableExtra(MainFragment.MOVIE_DATA);

        imgMovie = (ImageView) findViewById(R.id.img_movie);
        tvMovieName = (TextView) findViewById(R.id.tv_movie_name);
        tvYear = (TextView) findViewById(R.id.tv_year);
        tvRate = (TextView) findViewById(R.id.tv_rate);
        tvOverview = (TextView) findViewById(R.id.tv_overview);

        Picasso.with(this).load("http://image.tmdb.org/t/p/w342" +
                mMovieData.getPosterPath()).into(imgMovie);

        tvMovieName.setText(mMovieData.getTitle());
        tvYear.setText(mMovieData.getReleaseDate());
        tvRate.setText(mMovieData.getVoteAverage() + " / 10");
        tvOverview.setText(mMovieData.getOverview());
    }
}
