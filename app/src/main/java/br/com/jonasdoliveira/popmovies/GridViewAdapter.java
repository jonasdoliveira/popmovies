package br.com.jonasdoliveira.popmovies;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

/**
 * Created by Jonas on 13/12/2016.
 */

class GridViewAdapter extends ArrayAdapter {
    private Context mContext;
    private int mLayoutResourceId;
    private ArrayList<MovieData> mMovies;

    GridViewAdapter(Context context, int resource, ArrayList<MovieData> movies) {
        super(context, resource, movies);
        mLayoutResourceId = resource;
        mContext = context;
        mMovies = movies;
    }

    private static class ViewHolder {
        ImageView imageView;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (view == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            view = inflater.inflate(mLayoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.imageView = (ImageView) view.findViewById(R.id.grid_image_item);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        String path = mMovies.get(position).getPosterPath();
        Picasso.with(mContext).load("http://image.tmdb.org/t/p/w185" + path).into(holder.imageView);
        return view;
    }
}
